package tk.labyrinth.pandora.jobs.model;

import lombok.Value;

import java.time.Instant;

@Value
public class JobExecutionLogEntry {

	Instant happenedAt;

	String message;

	Integer priority;
}
