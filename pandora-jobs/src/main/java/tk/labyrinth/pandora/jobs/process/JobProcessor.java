package tk.labyrinth.pandora.jobs.process;

public interface JobProcessor<P> {

	void process(P parameters);
}
