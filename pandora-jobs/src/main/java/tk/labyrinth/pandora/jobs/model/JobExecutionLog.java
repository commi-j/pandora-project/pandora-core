package tk.labyrinth.pandora.jobs.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import tk.labyrinth.pandora.jobs.process.JobProcessor;
import tk.labyrinth.pandora.model.Document;

import java.util.List;

// NOTE: Copy these annotations to inheriting classes.
@Data // @EqualsAndHashCode, @Getter, @ToString
@FieldDefaults(level = AccessLevel.PRIVATE) // @Value equivalent
@NoArgsConstructor(access = AccessLevel.PROTECTED) // Default constructor for utility libraries, not for developers.
@Setter(AccessLevel.NONE) // Effectively immutable
@SuperBuilder(toBuilder = true) // Constructed by developers via builders
public class JobExecutionLog<P> extends Document {

	List<JobExecutionLogEntry> logEntries;

	String initiator;

	P parameters;

	Class<? extends JobProcessor<P>> processorType;
}
