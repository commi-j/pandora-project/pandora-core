package tk.labyrinth.pandora.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.Id;

import java.util.UUID;

// NOTE: Copy these annotations to inheriting classes.
@Data // @EqualsAndHashCode, @Getter, @ToString
@FieldDefaults(level = AccessLevel.PRIVATE) // @Value equivalent
@NoArgsConstructor(access = AccessLevel.PROTECTED) // Default constructor for utility libraries, not for developers.
@Setter(AccessLevel.NONE) // Effectively immutable
@SuperBuilder(toBuilder = true) // Constructed by developers via builders
public abstract class Document {

	// FIXME: Must not depend on MongoDB. Find the way to declare this field as Mongo ID in other way.
	@Id
	UUID uid;

	public abstract DocumentBuilder<?, ?> toBuilder();
}
