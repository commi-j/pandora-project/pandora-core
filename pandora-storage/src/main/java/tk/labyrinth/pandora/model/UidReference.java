package tk.labyrinth.pandora.model;

import lombok.Value;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Objects;
import java.util.UUID;

/**
 * Core Reference that identifies {@link Document} by combination of its {@link Class Type} and {@link UUID Uid}.<br>
 *
 * @param <E> Entity
 */
@Value
public class UidReference<E extends Document> {

	Class<E> type;

	UUID uid;

	/**
	 * Arguments of this constructor are effectively @{@link Nonnull Nonnull}.<br>
	 * They are @{@link Nullable} only in this private constructor for technical purpose.<br>
	 *
	 * @param type type of referenced Entity
	 * @param uid  uid of referenced Entity
	 */
	private UidReference(Class<E> type, @Nullable UUID uid) {
		this.type = Objects.requireNonNull(type, "type");
		this.uid = Objects.requireNonNull(uid, "uid");
	}

	/**
	 * Provided entity may be null or new (have no uid).<br>
	 * Formally:<br>
	 * entity == null {@literal ->} null<br>
	 * entity.uid == null {@literal ->} null<br>
	 *
	 * @param entity nullable
	 * @param <E>    Entity
	 *
	 * @return nullable
	 */
	@Nullable
	public static <E extends Document> UidReference<E> fromAny(@Nullable E entity) {
		return entity != null ? fromNewable(entity) : null;
	}

	/**
	 * Provided entity may not be null but may be new (have no uid).<br>
	 * Formally:<br>
	 * entity == null {@literal ->} fail<br>
	 * entity.uid == null {@literal ->} null<br>
	 *
	 * @param entity non-null with nullable uid
	 * @param <E>    Entity
	 *
	 * @return nullable
	 */
	@Nullable
	public static <E extends Document> UidReference<E> fromNewable(E entity) {
		Objects.requireNonNull(entity, "entity");
		//
		return entity.getUid() != null ? fromOld(entity) : null;
	}

	/**
	 * Provided entity may be null but may not be new (have no uid).<br>
	 * Formally:<br>
	 * entity == null {@literal ->} null<br>
	 * entity.uid == null {@literal ->} fail<br>
	 *
	 * @param entity nullable
	 * @param <E>    Entity
	 *
	 * @return nullable
	 */
	@Nullable
	public static <E extends Document> UidReference<E> fromNullable(@Nullable E entity) {
		return entity != null ? fromOld(entity) : null;
	}

	/**
	 * Provided entity may not be null and must be old (have uid).<br>
	 * Formally:<br>
	 * entity == null {@literal ->} fail<br>
	 * entity.uid == null {@literal ->} fail<br>
	 *
	 * @param entity non-null
	 * @param <E>    Entity
	 *
	 * @return non-null
	 */
	@SuppressWarnings("unchecked")
	public static <E extends Document> UidReference<E> fromOld(E entity) {
		Objects.requireNonNull(entity, "entity");
		//
		return new UidReference<>((Class<E>) entity.getClass(), entity.getUid());
	}

	/**
	 * type == null {@literal ->} fail<br>
	 * uid == null {@literal ->} fail<br>
	 *
	 * @param type non-null
	 * @param uid  non-null
	 * @param <E>  Entity
	 *
	 * @return non-null
	 */
	public static <E extends Document> UidReference<E> of(Class<E> type, UUID uid) {
		return new UidReference<>(type, uid);
	}
}
